package com.example.task_3_3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.task_3_3.interfaces.ArithmeticOperations;

public class MainActivity extends AppCompatActivity implements ArithmeticOperations {

    EditText firstNumber;
    EditText secondNumber;
    EditText operation;

    Button calc;
    TextView result;

    Switch accuracy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumber = findViewById(R.id.number1);
        secondNumber = findViewById(R.id.number2);
        operation = findViewById(R.id.operation);

        calc = findViewById(R.id.calc);
        result = findViewById(R.id.result);

        accuracy = findViewById(R.id.accuracy);

//      calc.setOnClickListener(this::calc);

        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calc(view);
            }

        });
    }

    public void calc(View view) {

        int firstNumberInt = Integer.parseInt(firstNumber.getText().toString().trim());
        int secondNumberInt = Integer.parseInt(secondNumber.getText().toString().trim());

        switch(operation.getText().toString().trim()){

            case ArithmeticOperations.ADDITION: {

                double resultDouble = additionNumbers(firstNumberInt, secondNumberInt);
                result.setText(accuracy.isChecked() ? String.valueOf(resultDouble) : String.valueOf((int) resultDouble));

                break;
            }

            case ArithmeticOperations.SUBTRACTION: {

                double resultDouble = subtractionNumbers(firstNumberInt, secondNumberInt);
                result.setText(accuracy.isChecked() ? String.valueOf(resultDouble) : String.valueOf((int) resultDouble));

                break;
            }

            case ArithmeticOperations.MULTIPLICATION: {

                double resultDouble = multiplicationNumbers(firstNumberInt, secondNumberInt);
                result.setText(accuracy.isChecked() ? String.valueOf(resultDouble) : String.valueOf((int) resultDouble));

                break;
            }

            case ArithmeticOperations.DIVISION: {
                double resultDouble = divisionNumbers(firstNumberInt, secondNumberInt);
                result.setText(accuracy.isChecked() ? String.valueOf(resultDouble) : String.valueOf((int) resultDouble));

                break;
            }

            default:
                break;
        }
    }

    @Override
    public Double additionNumbers(Integer numberFirst, Integer numberSecond) {
        return (double)numberFirst + (double)numberSecond;
    }

    @Override
    public Double subtractionNumbers(Integer numberFirst, Integer numberSecond) {
        return (double)numberFirst - (double)numberSecond;
    }

    @Override
    public Double multiplicationNumbers(Integer numberFirst, Integer numberSecond) {
        return (double)numberFirst * (double)numberSecond;
    }

    @Override
    public Double divisionNumbers(Integer numberFirst, Integer numberSecond) {

        double resultDouble = 0;

        if (numberSecond == 0)
            //           Toast.makeText(this, "Second value can not be 0", Toast.LENGTH_LONG).show();

            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Second value can not be 0")
//            .setCancelable(false) // нескрываемый диалог :-))
                    // нужно обязательно выбрать какой-то вариант
//            .setPositiveButton("OK", null) // диалог закроется
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            secondNumber.setText("");
                        }
                    }).show();
        else {
            resultDouble = ((double) numberFirst / (double) numberSecond);
//            result.setText(String.valueOf(firstNumberInt + secondNumberInt));
        }
        return resultDouble;
    }
}

