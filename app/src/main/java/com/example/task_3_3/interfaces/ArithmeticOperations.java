package com.example.task_3_3.interfaces;

public interface ArithmeticOperations {
    String ADDITION = "+";
    String SUBTRACTION = "-";
    String MULTIPLICATION = "*";
    String DIVISION = "/";

    Double additionNumbers(Integer numberFirst, Integer numberSecond);
    Double subtractionNumbers(Integer numberFirst, Integer numberSecond);
    Double multiplicationNumbers(Integer numberFirst, Integer numberSecond);
    Double divisionNumbers (Integer numberFirst, Integer numberSecond);
}
